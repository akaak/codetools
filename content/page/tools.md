---
title: Tools
subtitle: Tools page descriptions - subtitle
comments: false
categories: ["tools"]
---


Get a full fake REST API with zero coding in less than 30 seconds (seriously)
https://github.com/typicode/json-server

Small JSON database for Node, Electron and the browser. Powered by Lodash. ⚡ 
https://github.com/typicode/lowdb

Fake REST API endpoints
- https://jsonplaceholder.typicode.com/users 

Fake online REST server for teams
https://my-json-server.typicode.com/

#tools. Plunker is the best tool to prototype, experiment, share and debug your ideas on the web platform. 
https://plnkr.co/about/features
like http://www.codeskulptor.org/ 
