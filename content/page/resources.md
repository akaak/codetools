---
title: Resources
subtitle: Resources page description
comments: false
categories: ["tools", "learning"]
---




Mozilla Developer Guides
- https://developer.mozilla.org/en-US/docs/Web/Guide

Ajax
- https://developer.mozilla.org/en-US/docs/Web/Guide/AJAX

Ajax getting started: https://developer.mozilla.org/en-US/docs/Web/Guide/AJAX/Getting_Started 

## JavaScript Learning

The Modern JavaScript Tutorial
https://javascript.info/


MOZILLA Starting Point for JavaScript	
https://developer.mozilla.org/en-US/docs/Web/JavaScript 

https://masteringjs.io/all -- tutorials


** JS CLASSES:  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes 

JS Hoisting: https://developer.mozilla.org/en-US/docs/Glossary/Hoisting 

JavaScript Books:
- JavaScript: The Definitive Guide by David Flanagan
- Eloquent JavaScript by Marijn Haverbeke -- https://eloquentjavascript.net/ 
- JavaScript Patterns by Stoyan Stefanov
- Writing Maintainable JavaScript by Nicholas Zakas
- JavaScript: The Good Parts by Douglas Crockford


VueJs with interactive screencasts
https://scrimba.com/scrim/cQ3QVcr?pl=pXKqta

https://scrimba.com/
Interactive Screencasts -- CWS?!   ==> built using https://www.imba.io/




*Related:*
  - [articles](/page/articles)
  - [tools](/page/tools)
